print('Seja bem-vindo ao sistema da montanha-russa do palhaço louco, para ver se a pessoa esta apta a entrar dê suas informações!')
print('Nao coloque espaços inuteis no nome pois o sistema ira negar a entrada')

def DadosEntrada():
    global nome
    global altura
    global acompanhantes

    nome = str(input('Qual o nome completo? '))
    altura = float(input('Qual a sua altura? '))
    acompanhantes = int(input("Quantos acompanhantes estão com ela? "))
    print('-=-' * 20)
    return nome, altura, acompanhantes

def resultado():
    cabines = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    pessoas = 0
    contador = -1

    while pessoas < 1000:
        DadosEntrada()
        if not (((nome.strip().count(' ') == 2) and (altura > 1.61) and (altura < 2.55) and (acompanhantes > 0)) or ((altura == 1.50) and (acompanhantes == 4))):
            print('A(O) {} não está apto(a) a entrar na roda gigante! '.format(nome.title().strip()))
        else:
            pessoas += 1
            contador += 1
            if contador > 8:
                contador = -1
            print('Entre na cabine', cabines[contador])  # informa qual cabine está
            print('-=-' * 20)
    print('{} venceu o prêmio do palhaço louco, passe o local para que possa retirar o mesmo'.format(nome.title().strip()))

resultado()