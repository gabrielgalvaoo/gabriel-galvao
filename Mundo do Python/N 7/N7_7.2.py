from time import sleep
def ListaConvidado():
    global convidado
    convidado = []
    convidado.append(input('Qual o 1º convidado? '))
    convidado.append(input('Qual o 2º convidado? '))
    convidado.append(input('Qual o 3º convidado? '))

ListaConvidado()

def NaoVaiPoderIr():
    global NaoPodeIr
    NaoPodeIr = int(input('''Alguem desistiu de ir para o jantar?
Digite:
    [ 2 ] para "Não"
    [ 1 ] para "Sim"
    Opção = '''))
    if NaoPodeIr == 2:
        quit(print('Ok, até a proxima'))
    elif NaoPodeIr == 1:
        pessoa_desistiu = str(input('Qual pessoa desistiu de jantar? '))
        convidado.remove(pessoa_desistiu)
        print('{} desistiu de ir'.format(pessoa_desistiu.title()))

        convidado.append(input('Quem você deseja convidar no lugar da(o) {}? '.format(pessoa_desistiu.title())))
    else:
        quit(print('Opção invalida, execute o programa novamente!'))

NaoVaiPoderIr()

def Mensagem():
    print('Preparando convite')
    sleep(3)
    for i in convidado:
        print('{} estou fazendo um convite para vir jantar conosco, aceita?\n'.format(i))
        sleep(1)
Mensagem()