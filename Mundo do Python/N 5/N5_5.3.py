print('Calculadora simples entre dois valores')
num1 = float(input('Dígite o primeiro número: '))
num2 = float(input('Dígite o segundo número: '))

soma = num1 + num2
sub  = num1 - num2
mult = num1 * num2
div  = num1 / num2

print('''\nO resultado dos valores são:\n
Adição        {:.2f}
subtração     {:.2f}
Divisão       {:.2f}
multiplicação {:.2f}'''.format(soma, sub, div, mult))