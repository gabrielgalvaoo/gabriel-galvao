num     = int(input('Dígite un número para saber se ele é primo: '))
num_div = 0
for cont in range(1, num + 1):
    if num % cont == 0:
        num_div += 1

if num_div == 2:
    print('O número é primo!')
else:
    print('O número não é primo!')