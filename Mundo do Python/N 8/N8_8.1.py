mundo = ['orlando', 'africa', 'japão', 'paris', 'espanha']
print('-=' * 25)
print('Lista Original')
print(mundo)
print('-=' * 25)

print('Lista Ordenada')
print(sorted(mundo))
print('-=' * 25)

print('Lista ordenada inversa')
print(sorted(mundo, reverse = True))
print('-=' * 25)

print('Lista original novamente')
print(mundo)
print('-=' * 25)

print('Lista invertida usando reverse')
mundo.reverse()
print(mundo)
print('-=' * 25)

print('Lista invertida usando reverse')
mundo.reverse()
print(mundo)
print('-=' * 25)

print('Lista ordem alfabetica usando sort')
mundo.sort()
print(mundo)
print('-=' * 25)

print('Lista invertida usando sort')
mundo.sort(reverse = True)
print(mundo)
print('-=' * 25)