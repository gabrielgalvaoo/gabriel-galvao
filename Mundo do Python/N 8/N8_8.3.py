carro = ['bora', 'doblo', 'astra', 'escort', 'corolla']
print('-=' * 25)
print('Lista Original')
print(carro)
print('-=' * 25)

print('Lista Ordenada')
print(sorted(carro))
print('-=' * 25)

print('Lista ordenada inversa')
print(sorted(carro, reverse = True))
print('-=' * 25)

print('Lista original novamente')
print(carro)
print('-=' * 25)

print('Lista invertida usando reverse')
carro.reverse()
print(carro)
print('-=' * 25)

print('Lista invertida usando reverse')
carro.reverse()
print(carro)
print('-=' * 25)

print('Lista ordem alfabetica usando sort')
carro.sort()
print(carro)
print('-=' * 25)

print('Lista invertida usando sort')
carro.sort(reverse = True)
print(carro)
print('-=' * 25)