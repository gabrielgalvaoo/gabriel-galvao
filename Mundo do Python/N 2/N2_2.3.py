cebolas          = int(input('Dígite o número total de cebolas: '))
cebolas_na_caixa = int(input('Dígite o número de cebolas que estão na caixa: '))
espaco_caixa     = int(input('Dígite o número de cebolas que cabem em cada caixa: '))
caixas           = int(input('Digíte o número de caixas existentes: '))
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
caixas_vazias = caixas - (cebolas_na_caixa / espaco_caixa)
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

print('Existem {} cebolas encaixotadas'.format(cebolas_na_caixa))
print('Existem {} cebolas sem caixa'.format(cebolas_fora_da_caixa))
print('Em cada caixa cabem {} cebolas'.format(espaco_caixa))
print('Ainda temos {:.0f} caixas vazias'.format(caixas_vazias))
print('Então precisamos de {:.0f} caixas para empacotar todas as cebolas'.format(caixas_necessarias))