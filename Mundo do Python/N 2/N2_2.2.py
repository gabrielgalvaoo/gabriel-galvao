#valor atribuido para cebolas
cebolas          = 300
#Valor atribuido para cebolas na caixa
cebolas_na_caixa = 120
#Valor atribuido ao espaço que tem na caixa
espaco_caixa     = 5
#Valor atribuido ao número total de caixas
caixas           = 60
#Valor atribuido para cebolas fora da caixa é igual ao do número total de cebolas menos as cebolas que estão na caixa
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
#Valor atribuido para caixas vazias é igual ao número total de caixas menos a divisão de cebolas que estão na caixa e o espaço total da caixa
caixas_vazias = caixas - (cebolas_na_caixa / espaco_caixa)
#valor atribuido para caixa necessarias é igual ao numero de cebolas fora da caixa dividido pelo espaço total da caixa
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

#Mostrar na tela o número de cebolas encaixotadas
print('Existem {} cebolas encaixotadas'.format(cebolas_na_caixa))
#Mostrar na tela o número de cebolas sem caixa
print('Existem {} cebolas sem caixa'.format(cebolas_fora_da_caixa))
#Mostrar na tela o número de cebolas que cabe em cada caixa
print('Em cada caixa cabem {} cebolas'.format(espaco_caixa))
#Mostrar na tela o número restante de caixas vazias
print('Ainda temos {} caixas vazias'.format(caixas_vazias))
#Mostrar na tela o número de caixas necessarias para empacotar todas as cebolas
print('Então precisamos de {} caixas para empacotar todas as cebolas'.format(caixas_necessarias))