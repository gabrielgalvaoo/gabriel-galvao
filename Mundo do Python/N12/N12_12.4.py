from time import sleep
def MostraTupla():
    print('Valores Iniciais da tupla')

    sleep(2)

    comidas = ('arroz', 'feijao', 'frango', 'carne', 'salada')
    for comida in comidas:
        print('\tcardápio: {}'.format(comida))

    print('\n')
    sleep(2)

    print('Valores alterados da tupla')

    sleep(1)

    comidas = ('Arroz', 'Feijão', 'Frango', 'Peixe', 'Macarrão')
    for comida in comidas:
        print('\tNovo cardápio: {}'.format(comida))

    print('\n')
    sleep(1)

    print('tentando mudar valor dentro da tupla')
    sleep(5)
    comidas[0] = 'mamão'

MostraTupla()