from time import sleep


def ListaConvidado():
    global convidado
    convidado = []
    convidado.append(input('Qual o nome 1º convidado? '))
    convidado.append(input('Qual o nome 2º convidado? '))
    convidado.append(input('Qual o nome 3º convidado? '))


ListaConvidado()


def NaoVaiPoderIr():
    global NaoPodeIr
    NaoPodeIr = int(input('''Alguem desistiu de ir para o jantar?
Digite:
    [ 2 ] para "Não"
    [ 1 ] para "Sim"
    Opção = '''))
    if NaoPodeIr == 2:
        return
    elif NaoPodeIr == 1:
        pessoa_desistiu = str(input('Qual pessoa desistiu de jantar? '))
        convidado.remove(pessoa_desistiu)
        print('{} desistiu de ir'.format(pessoa_desistiu.title()))

        convidado.append(input('Quem você deseja convidar no lugar da(o) {}? '.format(pessoa_desistiu.title())))
    else:
        quit(print('Opção invalida, execute o programa novamente!'))


NaoVaiPoderIr()


def Mensagem():
    print('\nPreparando convite\n')
    sleep(3)
    for i in convidado:
        print('{} estou fazendo um convite para vir jantar conosco, aceita?\n'.format(i))
        sleep(1)


Mensagem()


def AumentouAMesa():
    print('Foi concedido a nós uma mesa maior, vou chamar mais 3 pessoas!\n')

    convidado.append(input('Qual o nome do 4º convidado? '))
    convidado.append(input('Qual o nome do 5º convidado? '))
    convidado.append(input('Qual o nome do 6º Convidado? '))

    Mensagem()

AumentouAMesa()

def slice():
    print('Os 3 primeiros da lista são: {}'.format(convidado[0:3]))
    print('Os 3 ultimos da lista são: {}'.format(convidado[-3:]))
    print('Os 3 do meio são: {}'.format(convidado[2:5]))

slice()