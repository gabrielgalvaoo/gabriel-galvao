from time import sleep


def ListaConvidado():
    global convidados
    convidados = []
    convidados.append(input('Qual o nome 1º convidado? '))
    convidados.append(input('Qual o nome 2º convidado? '))
    convidados.append(input('Qual o nome 3º convidado? '))


ListaConvidado()


def NaoVaiPoderIr():
    global NaoPodeIr
    NaoPodeIr = int(input('''Alguem desistiu de ir para o jantar?
Digite:
    [ 2 ] para "Não"
    [ 1 ] para "Sim"
    Opção = '''))
    if NaoPodeIr == 2:
        return
    elif NaoPodeIr == 1:
        pessoa_desistiu = str(input('Qual pessoa desistiu de jantar? '))
        convidados.remove(pessoa_desistiu)
        print('{} desistiu de ir'.format(pessoa_desistiu.title()))

        convidados.append(input('Quem você deseja convidar no lugar da(o) {}? '.format(pessoa_desistiu.title())))
    else:
        quit(print('Opção invalida, execute o programa novamente!'))


NaoVaiPoderIr()

def AumentouAMesa():
    print('Foi concedido a nós uma mesa maior, vou chamar mais 3 pessoas!\n')

    convidados.append(input('Qual o nome do 4º convidado? '))
    convidados.append(input('Qual o nome do 5º convidado? '))
    convidados.append(input('Qual o nome do 6º Convidado? '))
    print('\n')

AumentouAMesa()

def slice():
    print('Os primeiros da lista são:')

    for primeirosconvidados in convidados[0:3]:
        print(primeirosconvidados.title())
    print('\n')

    sleep(1)

    print('Os ultimos da lista são: ')

    for ultimosconvidados in convidados[-3:]:
        print(ultimosconvidados.title())
    print('\n')

    sleep(1)

    print('A metade da lista são: ')

    for meioconvivados in convidados[2:5]:
        print(meioconvivados.title())
    print('\n')

    sleep(5)

slice()