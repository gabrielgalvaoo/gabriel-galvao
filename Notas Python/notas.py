import  sys
print('Dígite as notas de 0 a 10 e o peso de 0 a 5')
nome    = str(input('Digite o nome do aluno: '))
nota1   = float(input('Dígite a primeira nota: '))
peso1   = int(input('Digite o peso da primeira nota: '))
nota2   = float(input('Dígite a segunda nota: '))
peso2   = int(input('Digite o peso da segunda nota: '))
nota3   = float(input('Dígite a terceira nota: '))
peso3   = int(input('Digite o peso da terceira nota: '))
nota4   = float(input('Dígite a quarta nota: '))
peso4   = int(input('Digite o peso da quarta nota: '))
nota5   = float(input('Dígite a quinta nota: '))
peso5   = int(input('Digite o peso da quinta nota: '))
nota6   = float(input('Dígite a sexta nota: '))
peso6   = int(input('Digite o peso da sexta nota: '))
nota7   = float(input('Dígite a setima nota: '))
peso7   = int(input('Digite o peso da setima nota: '))
nota8   = float(input('Dígite a oitava nota: '))
peso8   = int(input('Digite o peso da oitava nota: '))
nota9   = float(input('Dígite a nona nota: '))
peso9   = int(input('Digite o peso da nona nota: '))
nota10  = float(input('Dígite a décima nota: '))
peso10  = int(input('Digite o peso da decima nota: '))

if peso1 or peso2 or peso3 or peso4 or peso5 or peso6 or peso7 or peso8 or peso9 or peso10 < 6:
    print('Vamos calcular a media!')
    sys.exit('Não tem como calcular!')

media1 = (nota1 + nota2 + nota3 + nota4 + nota5 + nota6 + nota7 + nota8 + nota9 + nota10) / 10
media2 = (nota1 * peso1) + (nota2 * peso2) + (nota3 * peso3) + (nota4 * peso4) + (nota5 + peso5) + (nota6 * peso6) + (nota7 * peso7) + (nota8 * peso8) + (nota9 * peso9) + (nota10 * nota10)
mediapeso = media2 / (peso1 + peso2 + peso3 + peso4 + peso5 + peso6 + peso7 + peso8 + peso9 + peso10)

print('A media do(a) {} sem os pesos é de {:.2f}'.format(nome, media1))
print('A média do(a) {} sem os pesos é de {:.2f}'.format(nome, mediapeso))