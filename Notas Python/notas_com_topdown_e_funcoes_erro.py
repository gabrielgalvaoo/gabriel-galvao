import sys
def Nome_aluno():
    return input('Digite o nome do(a) aluno que deseja fazer a media: ')

nome = Nome_aluno()

def Numero_de_notas():
    return int(input("Quantidade de notas: "))

num_notas = Numero_de_notas()

def Nota_aluno():
    NotaAluno = float(input('Digite a nota do aluno: '))
    if NotaAluno > 10:
        sys.exit("Dado inválido, tente novamente")
    return  NotaAluno


def Peso_nota_aluno():
    PesoAluno = int(input('Dígite o peso da nota: '))
    if PesoAluno > 5:
        sys.exit("Dado inválido, tente novamente")
    return PesoAluno

def Resultado():
    soma_notas       = 0
    soma_pesos_notas = 0
    soma_pesos       = 0

    for i in range(num_notas):
        nota = Nota_aluno()
        peso = Peso_nota_aluno()
        # adiciona a soma das notas
        soma_notas = soma_notas + nota

        # adiciona a soma das notas com peso
        soma_pesos_notas = soma_pesos_notas + (nota * peso)

        # adiciona a soma de pesos
        soma_pesos = soma_pesos + peso

    media = soma_notas / num_notas
    mediapond = soma_pesos_notas / soma_pesos

    print("\n")
    print("-=" * 11)
    print('''A média do(a) do Aluno {} sem os pesos é de: {}
E com os pesos é de {}'''.format(nome, media, mediapond))

Resultado()

def Opcao_para_fazer_a_media_novamente():
    print('''Para realizar novamente a média dos alunos escolha uma das opções:
[ 1 ] SIM
[ 2 ] Não''')
    return int(input('Qual a opção escolhida? '))

opcao = Opcao_para_fazer_a_media_novamente()



def Escolha_de_opcao():
    if opcao == 1:
        Nome_aluno()
        Numero_de_notas()
        Resultado()
        Opcao_para_fazer_a_media_novamente()
        Escolha_de_opcao()
    elif opcao == 2:
        sys.exit("Ok,Até a proxima")

Escolha_de_opcao()






