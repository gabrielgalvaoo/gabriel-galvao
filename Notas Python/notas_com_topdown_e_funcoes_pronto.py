import sys
def Nome_aluno():
    global nome
    nome = 0
    nome = input('Digite o nome do(a) aluno que deseja fazer a media: ')
    return  nome

def Numero_de_notas():
    global num_notas
    num_notas = 0
    num_notas = int(input("Quantidade de notas: "))
    return num_notas


def Nota_aluno():
    NotaAluno = float(input('Digite a nota do aluno: '))
    if 10 < NotaAluno < 0:
        sys.exit("Dado inválido, tente novamente")
    return  NotaAluno


def Peso_nota_aluno():
    PesoAluno = int(input('Dígite o peso da nota: '))
    if 5 < PesoAluno < 0:
        sys.exit("Dado inválido, tente novamente")
    return PesoAluno


def Opcao_para_fazer_a_media():
    global opcao
    opcao = 0
    print('''Para realizar a média dos alunos escolha uma das opções:
[ 1 ] SIM
[ 2 ] Não''')
    opcao = int(input('Qual a opção escolhida? '))
    if opcao != 1 and opcao != 2:
        print('Opção invalida')
    return opcao


Opcao_para_fazer_a_media()


def Resultado():
    soma_notas       = 0
    soma_pesos_notas = 0
    soma_pesos       = 0
    Nome_aluno()

    for i in range(Numero_de_notas()):
        nota = Nota_aluno()
        peso = Peso_nota_aluno()
        # adiciona a soma das notas
        soma_notas = soma_notas + nota

        # adiciona a soma das notas com peso
        soma_pesos_notas = soma_pesos_notas + (nota * peso)

        # adiciona a soma de pesos
        soma_pesos = soma_pesos + peso

        media = soma_notas / num_notas
        mediapond = soma_pesos_notas / soma_pesos

    print("\n")
    print("-=" * 11)
    print('''A média do(a) do Aluno {} sem os pesos é de: {:.2f}
E com os pesos é de {:.2f}'''.format(nome, media, mediapond))


def Escolha_de_opcao():
    while (opcao == 1):
        Resultado()
        Opcao_para_fazer_a_media()

Escolha_de_opcao()


