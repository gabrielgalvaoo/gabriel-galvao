from random import randint
from time import sleep


#Números aleatorios juntando com os itens.
itens = ('Pedra', 'Papel', 'Tesoura')
computador = randint(0, 2)


def QualoNome():
    return input('Dígite o nome do jogador:  ')


def EscolhaDeJogadas():
    print ('''Suas jogadas:
    [ 0 ] PEDRA 
    [ 1 ] PAPEL
    [ 2 ] TESOURA''')


def JogadaUsuario():
    return int(input('Qual é a sua jogada? '))


nome    = QualoNome()
EscolhaDeJogadas()
jogada = JogadaUsuario()


def condicoes():
    print('-=' * 13)
    if jogada != 0 and jogada != 1 and jogada != 2:
        print('JOGADA INVALIDA \nJogue novamente')
        print('-=' * 13)
        quit()
    else:
        print('JO')
        sleep(1)
        print('KEN')
        sleep(1)
        print('PO')
        sleep(1)
        print('O computador jogou {}'.format(itens[computador]))
        print('O {} jogou {}'.format(nome.title(), itens[jogada]))
        print('-=' * 13)


condicoes()


def Resultados():
    if computador == 0:  # Computador jogou PEDRA
        if jogada == 0:
            print('FOI EMPATE')
        elif jogada == 1:
            print('{} VENCEU'.format(nome.title()))
        elif jogada == 2:
            print('COMPUTADOR VENCEU')

    elif computador == 1:  # Computador jogou PEDRA
        if jogada == 0:
            print('COMPUTADOR VENCEU')
        elif jogada == 1:
            print('FOI EMPATE')
        elif jogada == 2:
            print('{} VENCEU'.format(nome.title()))

    elif computador == 2:  # Computador jogou TESOURA
        if jogada == 0:
            print('{} VENCEU'.format(nome.title()))
        elif jogada == 1:
            print('COMPUTADOR VENCEU')
        elif jogada == 2:
            print('FOI EMPATE')

Resultados()